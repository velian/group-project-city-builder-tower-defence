﻿using UnityEngine;
using System.Collections;

public class EnemySpawn : MonoBehaviour {
	
	public GameObject m_enemyPrefab;
	public Transform m_enemyTarget;
	
	public float m_enemyTick = 3;
	public float m_waveTimer = 10;
	
	public float m_startingHealth = 1;
	
	public int m_maxEnemies;
	public int m_numberOfEnemies;
	
	float m_enemyTickOrigin;
	float m_waveTimerOrigin;
	
	public int m_currentWave = 0;

	// Use this for initialization
	void Start ()
	{
		m_enemyTickOrigin = m_enemyTick;
		m_waveTimerOrigin = m_waveTimer;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(m_currentWave != 0)
		{
			if(m_enemyTick > 0)
			{
				m_enemyTick -= Time.deltaTime;
			}
			else
			{
				for(int i = 0; i < m_numberOfEnemies; i++)
				{
					SpawnEnemy();
				}
				
				m_enemyTick = m_enemyTickOrigin;
			}
		}
		
		if(m_waveTimer > 0)
		{
			m_waveTimer -= Time.deltaTime;
		}
		else
		{
			m_waveTimer = m_waveTimerOrigin;
			m_currentWave++;
			
			if(m_numberOfEnemies < m_maxEnemies)
			{
				m_numberOfEnemies++;
			}
			else
			{
				m_numberOfEnemies = 1;
			}
		}
	}
	
	void SpawnEnemy()
	{
		Vector3 pos = transform.position;
		GameObject m_enemy = (GameObject)Instantiate(m_enemyPrefab, pos, Quaternion.identity);
		m_enemy.GetComponent<Enemy>().m_target = m_enemyTarget;
		m_enemy.GetComponent<Enemy>().m_health = m_startingHealth + m_currentWave;
	}
}
