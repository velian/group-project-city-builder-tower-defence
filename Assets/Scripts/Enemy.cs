﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public Transform m_target;
	
	public float m_health;

	// Use this for initialization
	void Start () {
		GetComponent<NavMeshAgent>().SetDestination(m_target.position);
	}
	
	// Update is called once per frame
	void Update () {
		transform.rotation = Quaternion.Euler(new Vector3(90, 0, 0));
	}
	
	void OnTriggerEnter(Collider trigger)
	{
		if(trigger.name == "Enemy Target")
		{
			Destroy(this.gameObject);
		}
	}
}
