﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {

	public float m_speed;
	public float m_zoomSpeed;
	
	public float m_maxX;
	public float m_minX;
	public float m_maxY;
	public float m_minY;
	
	public float m_maxZoom;
	public float m_minZoom;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		HandleInput();
	}
	
	void HandleInput()
	{		
		//Camera movement
		if (Input.GetKey (KeyCode.W) && transform.position.z + m_speed * Time.deltaTime < m_maxY)
		{
			transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + m_speed * Time.deltaTime);
		}
		if (Input.GetKey (KeyCode.A) && transform.position.x - m_speed * Time.deltaTime > m_minX)
		{
			transform.position = new Vector3(transform.position.x - m_speed * Time.deltaTime, transform.position.y, transform.position.z);
		}
		if (Input.GetKey (KeyCode.S) && transform.position.z - m_speed * Time.deltaTime > m_minY)
		{
			transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - m_speed * Time.deltaTime);
		}
		if (Input.GetKey (KeyCode.D) && transform.position.x + m_speed * Time.deltaTime < m_maxX)
		{
			transform.position = new Vector3(transform.position.x + m_speed * Time.deltaTime, transform.position.y, transform.position.z);
		}
		
		//Camera zoom
		if (Input.GetAxis ("Mouse ScrollWheel") != 0)
		{
			if(Input.GetAxis ("Mouse ScrollWheel") > 0 && transform.position.y - m_zoomSpeed * Time.deltaTime > m_maxZoom)
			{
				transform.position = new Vector3(transform.position.x, transform.position.y - m_zoomSpeed * Time.deltaTime, transform.position.z);
			}
			else if(Input.GetAxis ("Mouse ScrollWheel") < 0 && transform.position.y + m_zoomSpeed * Time.deltaTime < m_minZoom)
			{
				transform.position = new Vector3(transform.position.x, transform.position.y + m_zoomSpeed * Time.deltaTime, transform.position.z );
			}
		}
	}
}
