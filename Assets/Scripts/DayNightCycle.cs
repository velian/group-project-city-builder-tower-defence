﻿using UnityEngine;
using System.Collections;

public class DayNightCycle : MonoBehaviour {

	public int m_rotationSpeed;
	public Light m_light;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		m_light.transform.Rotate(new Vector3(0, m_rotationSpeed * Time.deltaTime, 0));
	}
}
