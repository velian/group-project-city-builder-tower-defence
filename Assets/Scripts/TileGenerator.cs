﻿using UnityEngine;
using System.Collections;

public class TileGenerator : MonoBehaviour
{	
	public GameObject m_tilePrefab;

	public int m_rows;
	public int m_cols;
	
	public float m_spacing;

	public Sprite[] m_spriteList;
	
	public float m_tileTick;

	// Use this for initialization
	void Start () 
	{
		for (int x = 0; x < m_rows; x++)
		{
			for (int y = 0; y < m_cols; y++)
			{
				Vector3 pos = transform.position + new Vector3(x, 0, -y) * m_spacing;
				GameObject m_tile = (GameObject)Instantiate(m_tilePrefab, pos, m_tilePrefab.transform.rotation);
				m_tile.GetComponent<Tile>().m_timer = m_tileTick;
			}
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}
}
