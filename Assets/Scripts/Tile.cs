﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour
{
	TileManager.TILE_TYPE m_type;	
	Renderer m_renderer;
	
	public float m_timer;
	private float m_timerOrigin;

	// Use this for initialization
	void Start ()
	{
		m_type = TileManager.TILE_TYPE.NONE;
		m_renderer = GetComponent<Renderer>();
		m_timerOrigin = m_timer;
	}
	
	// Update is called once per frame
	void Update ()
	{
		//if(m_timer > 0 && m_type != TileManager.TILE_TYPE.NONE)
		//{
		//	m_timer -= Time.deltaTime;				
		//}
		//else if(m_type != TileManager.TILE_TYPE.NONE)
		//{
		//	GetComponent<ParticleSystem>().Play ();
		//	m_timer = m_timerOrigin;
		//}		
	}
	
	void OnMouseOver()
	{
		if(Input.GetMouseButton(0))
		{		
			if(m_type != GameObject.Find("TileManager").GetComponent<TileManager>().GetSelection())
			{
				m_type = GameObject.Find("TileManager").GetComponent<TileManager>().GetSelection();
				
				switch(m_type)
				{
					case (TileManager.TILE_TYPE.RESIDENTIAL):
					m_renderer.material.color = Color.green;
					GetComponent<ParticleSystem>().startColor = Color.green;
					GetComponent<ParticleSystem>().Play();
					GameObject.Find("TileManager").GetComponent<TileManager>().m_populationCount++;
					break;
					
					case (TileManager.TILE_TYPE.BUSINESS):
					m_renderer.material.color = Color.blue;
					GetComponent<ParticleSystem>().startColor = Color.blue;
					GetComponent<ParticleSystem>().Play();
					GameObject.Find("TileManager").GetComponent<TileManager>().m_businessCount++;
					break;
					
					case (TileManager.TILE_TYPE.INDUSTRIAL):
					m_renderer.material.color = Color.yellow;
					GetComponent<ParticleSystem>().startColor = Color.yellow;
					GetComponent<ParticleSystem>().Play();
					GameObject.Find("TileManager").GetComponent<TileManager>().m_industrialCount++;
					break;
					
					case (TileManager.TILE_TYPE.TURRET):
					m_renderer.material.color = Color.magenta;
					GetComponent<ParticleSystem>().startColor = Color.magenta;
					GetComponent<ParticleSystem>().Play();
					GameObject.Find("TileManager").GetComponent<TileManager>().m_turretCount++;
					break;
					
					case (TileManager.TILE_TYPE.NONE):
					m_renderer.material.color = Color.white;
					GetComponent<ParticleSystem>().startColor = Color.white;
					GetComponent<ParticleSystem>().Play();
					
					if(m_type == TileManager.TILE_TYPE.RESIDENTIAL)
						GameObject.Find("TileManager").GetComponent<TileManager>().m_populationCount--;
					else if(m_type == TileManager.TILE_TYPE.BUSINESS)
						GameObject.Find("TileManager").GetComponent<TileManager>().m_businessCount--;
					else if(m_type == TileManager.TILE_TYPE.INDUSTRIAL)
						GameObject.Find("TileManager").GetComponent<TileManager>().m_industrialCount--;
					else if(m_type == TileManager.TILE_TYPE.TURRET)
						GameObject.Find("TileManager").GetComponent<TileManager>().m_turretCount--;
						
					break;
				}
			}
		}
		else if(Input.GetMouseButton(1))
		{
			if(m_type != TileManager.TILE_TYPE.NONE)
			{
				m_type = TileManager.TILE_TYPE.NONE;
				m_renderer.material.color = Color.white;
				GetComponent<ParticleSystem>().startColor = Color.white;
				GetComponent<ParticleSystem>().Play();
			}
		}
	}
}
