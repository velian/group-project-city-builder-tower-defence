﻿using UnityEngine;
using System.Collections;

public class TileManager : MonoBehaviour
{
	public enum TILE_TYPE
	{
		RESIDENTIAL,
		BUSINESS,
		INDUSTRIAL,
		TURRET,
		NONE
	}
	
	TILE_TYPE m_currentSelection = TILE_TYPE.NONE;
	
	public float m_residentialPrice = 10;
	public float m_businessPrice = 20;
	public float m_industrialPrice = 30;
	public float m_turretPrice = 200;
	
	public float m_populationCount;
	public float m_businessCount;
	public float m_industrialCount;
	public float m_turretCount;
	
	public float m_totalFunds = 200;
	
	public float m_timer;
	float m_timerOrigin;

	// Use this for initialization
	void Start ()
	{
		m_timerOrigin = m_timer;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(m_timer > 0)
		{
			m_timer -= Time.deltaTime;
		}
		else
		{
			m_totalFunds += m_populationCount + (m_businessCount * 2) + (m_industrialCount * 3) - (m_turretCount * 5);
			m_timer = m_timerOrigin;
		}
	}
	
	public void SetSelection(TILE_TYPE _tileType)
	{
		m_currentSelection = _tileType;
	}
	
	public TILE_TYPE GetSelection()
	{
		return m_currentSelection;
	}
}
