﻿using UnityEngine;
using System.Collections;

public class DebugGUI : MonoBehaviour {

	void OnGUI()
	{
		if(GUI.Button (new Rect(0,0, 100, 25), "Set Residential"))
		{
			GameObject.Find("TileManager").GetComponent<TileManager>().SetSelection(TileManager.TILE_TYPE.RESIDENTIAL);
		}
		
		if(GUI.Button (new Rect(0,25, 100, 25), "Set Business"))
		{
			GameObject.Find("TileManager").GetComponent<TileManager>().SetSelection(TileManager.TILE_TYPE.BUSINESS);
		}
		
		if(GUI.Button (new Rect(0,50, 100, 25), "Set Industrial"))
		{
			GameObject.Find("TileManager").GetComponent<TileManager>().SetSelection(TileManager.TILE_TYPE.INDUSTRIAL);
		}
		
		if(GUI.Button (new Rect(0,75, 100, 25), "Set Turret"))
		{
			GameObject.Find("TileManager").GetComponent<TileManager>().SetSelection(TileManager.TILE_TYPE.TURRET);
		}
		
		if(GUI.Button (new Rect(0,100, 100, 25), "Set None"))
		{
			GameObject.Find("TileManager").GetComponent<TileManager>().SetSelection(TileManager.TILE_TYPE.NONE);
		}

		string formattedFunds = "Total Funds: " + GameObject.Find("TileManager").GetComponent<TileManager>().m_totalFunds.ToString();
		
		GUI.Label (new Rect(100, 0, 100, 100), formattedFunds.ToString());
	}
}
